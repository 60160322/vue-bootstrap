const userService = {
  userList: [
    { id: 1, name: 'Natcha', gender: 'F' },
    { id: 2, name: 'Piano', gender: 'F' }
  ],
  lastId: 3,
  addUser (user) {
    user.id = this.lastId++
    this.userList.push(user)
  },
  updateUser (user) {
    const index = this.userList.findIndex(item => item.id === user.id)
    this.userList.splice(index, 1, user)
  },
  deleteUser (user) {
    const index = this.userList.findIndex(item => item.id === user.id)
    this.userList.splice(index, 1)
  },
  getUsers () {
    return [...this.userList]
  }

}

// eslint-disable-next-line eol-last
export default userService